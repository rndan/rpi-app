#include <chrono>
#include <fstream>
#include <iostream>
#include <mqtt/async_client.h>
#include <string>
#include <thread>

int main(int argc, char** argv)
{
    using namespace std::chrono_literals;

    if(argc != 2)
    {
        std::cout << "Error: missing mqtt broker address" << std::endl;
        return -1;
    }

    const std::string temperatureFilePath{"/sys/class/thermal/thermal_zone0/temp"};
    std::ifstream temperatureFile;

    const std::string mqttBrokerAddr{argv[1]};
    const std::string mqttTopic{"temperature"};
    const int mqttQos{1};

    std::cout << "creating client" << std::endl;
    mqtt::async_client client(mqttBrokerAddr, "");
    std::cout << "connecting to broker" << std::endl;
    client.connect()->wait();
    mqtt::topic topic(client, mqttTopic, mqttQos);

    while(true)
    {
        // get current temperature
        std::string temperature{};
        temperatureFile.open(temperatureFilePath);
        if(temperatureFile.is_open() && temperatureFile.good())
        {
            temperatureFile >> temperature;
            std::cout << "CPU temp is " << temperature << std::endl;
        }
        else
        {
            const int fakeTemp{20000 + (rand() % 10000)};
            temperature = std::to_string(fakeTemp);
            std::cout << "failure, genarated random value of " << temperature << std::endl;
        }
        temperatureFile.close();

        // send to qmtt
        topic.publish(temperature)->wait();
        std::this_thread::sleep_for(1s);
    }

    client.disconnect()->wait();

    return 0;
}
